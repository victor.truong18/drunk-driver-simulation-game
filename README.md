# Drunk driving simulator

Wait are you really going to drive right now?

Seems like you are!

![Screenshot 2023-09-20 at 14.22.20.png](Drunk%20driving%20simulator%20de56fe8e96b44ffe905b2f506663ca38/Screenshot_2023-09-20_at_14.22.20.png)

Try to get to the end while keeping you eyes on the road…

Feeling dizzy?

![Screenshot 2023-09-20 at 14.26.16.png](Drunk%20driving%20simulator%20de56fe8e96b44ffe905b2f506663ca38/Screenshot_2023-09-20_at_14.26.16.png)

Anyway it’s late! Keep your eyes open and do your best to get back home!

![Screenshot 2023-09-20 at 14.28.01.png](Drunk%20driving%20simulator%20de56fe8e96b44ffe905b2f506663ca38/Screenshot_2023-09-20_at_14.28.01.png)