using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointController : MonoBehaviour
{
    public Transform Anchor;
    public Transform[] CheckPoints;
    public bool reachedEnd;

    private Transform target;
    private Transform arrow;
    private int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameObject instance = Instantiate(Resources.Load("Waypoint Arrow", typeof(GameObject))) as GameObject;
        instance.name = "Waypoint Arrow";
        instance = null;
        arrow = GameObject.Find("Waypoint Arrow").transform;
        arrow.localScale = new Vector3(0.25f, 0.25f, 0.25f);
        target = CheckPoints[0];
        reachedEnd = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (index >= CheckPoints.Length && !reachedEnd) {
            reachedEnd = true;
            Destroy(GameObject.Find("Waypoint Arrow"));
            return;
        } else if (reachedEnd) {
            return;
        } 
        if (target.gameObject.GetComponent<HitCheckpoint>().m_ReachedEnd) {
            index += 1;
            target = CheckPoints[index];
        }
        target.localPosition = Vector3.Lerp(target.localPosition, target.localPosition, 10 * Time.deltaTime);
        target.localRotation = Quaternion.Lerp(target.localRotation, target.localRotation, 10 * Time.deltaTime);

        arrow.position = new Vector3(Anchor.position.x, Anchor.position.y, Anchor.position.z);
        arrow.LookAt(target);
    }
}
