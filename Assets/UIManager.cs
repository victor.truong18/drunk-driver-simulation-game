using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.Vehicles.Car
{
public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject[] imageComponents;

    private CarInputsController carController;
    private CarInputsController.ApplyPenalty currentPenalty;

    void Start() {
        carController = player.GetComponent<CarInputsController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        CarInputsController.ApplyPenalty penalty = carController.currentPenalty;
        if (penalty == currentPenalty) { return; }

        if (penalty == null) {
            if (currentPenalty != null) {
                imageComponents[ComponentIndex(currentPenalty)].SetActive(false);

                currentPenalty = null;
            }
            return;
        }
        
        imageComponents[ComponentIndex(penalty)].SetActive(true);

        currentPenalty = penalty;
    }

    int ComponentIndex(CarInputsController.ApplyPenalty penalty) {
        int index = -1;
        
        if (penalty is CarInputsController.FullSpeedStraight) {
            index = 0;
        } else if (penalty is CarInputsController.Break) {
            index = 1;
        } else if (penalty is CarInputsController.DrunkASFPenalty) {
            index = 2;
        } else if (penalty is CarInputsController.SwapTurn) {
            index = 3;
        } else if (penalty is CarInputsController.FadeScreen) {
            index = 4;
        } else if (penalty is CarInputsController.SlowTurn) {
            index = 5;
        } else if (penalty is CarInputsController.TurnLeft) {
            index = 6;
        } else if (penalty is CarInputsController.TurnRight) {
            index = 7;
        }

        return index;
    }
}
}