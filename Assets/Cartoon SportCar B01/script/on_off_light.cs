﻿using UnityEngine;
using System.Collections;

public class on_off_light : MonoBehaviour
{

	public Light[] lights;
	public KeyCode keyboard;

	void Start() {
		foreach (Light light in lights) {
			light.enabled = true;
		}
	}
}

