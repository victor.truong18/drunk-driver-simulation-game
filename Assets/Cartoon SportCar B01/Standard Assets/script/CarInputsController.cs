using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Diagnostics;

namespace UnityStandardAssets.Vehicles.Car
    {
    [RequireComponent(typeof (CarController))]
    public class CarInputsController : MonoBehaviour
    {
        private CarController m_Car; // the car controller we want to use
        [SerializeField] private GameObject m_Canvas;

        [Tooltip("Time elapsed since last penalty")]
        [SerializeField] private float last;
        [Tooltip("Result of the random draw between 0 and 100")]
        [SerializeField] private float chance;
        [Tooltip("Cooldown between the end of the previous penalty and the next one.\n" +
         "Can be used to specify after how long we can start applying the first penalty.")]
        [SerializeField] private float cooldownTillNextPenalty = 10f;
        [Tooltip("Time duration of the current penalty")]
        [SerializeField] private float duration;
        [Tooltip("Current penalty (might be used to add ui indicators)")]
        [SerializeField] public ApplyPenalty currentPenalty;
        [Tooltip("List of all the possible penalty (might be used to add ui indicators)")]
        [SerializeField] private List<ApplyPenalty> penalties;
        [Tooltip("Indicates if we are ready to apply the next penalty")]
        [SerializeField] private bool readyForNextPenalty;
        [SerializeField] private float eventsChances;

        public void Awake() {
            m_Car = GetComponent<CarController>();
        }

        // Start is called before the first frame update
        void Start()
        {
            last = Time.time;
            chance = 0;

            penalties = new List<ApplyPenalty>();

            penalties.Add(new FullSpeedStraight());
            penalties.Add(new TurnRight());
            penalties.Add(new TurnLeft());
            penalties.Add(new Break());
            penalties.Add(new SwapTurn());
            penalties.Add(new SlowTurn());
            penalties.Add(new FadeScreen(m_Canvas.GetComponent<Fade>()));
            penalties.Add(new DrunkASFPenalty(gameObject.GetComponentInChildren(typeof(DrunkASF)) as DrunkASF));

            currentPenalty = null;
            duration = 0f;
            readyForNextPenalty = false;
        }

        // Update is called once per frame
        void Update()
        {
            float frameTime = Time.time;
            if (frameTime >= last + duration + cooldownTillNextPenalty) {
                chance = Random.Range(0, 100f);
                last = frameTime;
                readyForNextPenalty = true;
            }

            chooseNextPenalty();
        }

        public void chooseNextPenalty() {
            if (readyForNextPenalty) {
                // Chooses the next random penalty
                if (chance >= eventsChances) {
                    int index = Random.Range(0, penalties.Count);
                    currentPenalty = penalties[index];
                    cooldownTillNextPenalty = currentPenalty.cooldown;
                    duration = currentPenalty.duration;
                    readyForNextPenalty = false;
                    UnityEngine.Debug.Log(currentPenalty);
                }
                // If the odds were not with us we try again in 5 seconds
                else {
                    cooldownTillNextPenalty = 5f;
                    duration = 0f;
                }
            } else {
                if (Time.time < last + duration) {
                    currentPenalty.Apply(m_Car);
                    return;
                } else if (currentPenalty != null) {
                    currentPenalty.CancelPenalty(m_Car);
                    currentPenalty = null;
                }
            }

            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            float handbrake = CrossPlatformInputManager.GetAxis("Jump");
            m_Car.Move(h, v, v, handbrake);
        }
        
        // ***********************************************
        // ****************** PENALTIES ******************
        // ***********************************************

        // Every penalty must implement this interface
        public interface ApplyPenalty
        {
            // Duration of the penalty
            float duration
            {
                get;
            }

            // Cooldown before an other penalty can be applied
            float cooldown
            {
                get;
            }

            // Applies the penalty and calls car.Move()
            void Apply(CarController car);
            // Reverses any changes the penalty might have done
            void CancelPenalty(CarController car);
        }

        public class FullSpeedStraight : ApplyPenalty {
            public FullSpeedStraight() {
                duration = 5f;
                cooldown = 5f;
            }

            public float duration { get; }

            public float cooldown { get; }

            void ApplyPenalty.Apply(CarController car) {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");
                car.Move(h, 1f, 1f, handbrake);
            }

            void ApplyPenalty.CancelPenalty(CarController car) { return; }
        }

        public class TurnRight : ApplyPenalty {
            public TurnRight() {
                duration = 1f;
                cooldown = 1f;
            }

            public float duration { get; }

            public float cooldown { get; }

            void ApplyPenalty.Apply(CarController car) {
                float v = CrossPlatformInputManager.GetAxis("Vertical");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");

                car.Move(0.5f, v, v, handbrake);
            }

            void ApplyPenalty.CancelPenalty(CarController car) { return; }
        }

        public class TurnLeft : ApplyPenalty {
            public TurnLeft() {
                duration = 1f;
                cooldown = 1f;
            }

            public float duration { get; }

            public float cooldown { get; }

            void ApplyPenalty.Apply(CarController car) {
                float v = CrossPlatformInputManager.GetAxis("Vertical");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");

                car.Move(-0.5f, v, v, handbrake);
            }

            void ApplyPenalty.CancelPenalty(CarController car) { return; }
        }

        public class Break : ApplyPenalty {
            public Break() {
                duration = 1f;
                cooldown = 1f;
            }

            public float duration { get; }

            public float cooldown { get; }

            void ApplyPenalty.Apply(CarController car) {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");
                car.Move(h, -1f, -1f, handbrake);
            }

            void ApplyPenalty.CancelPenalty(CarController car) { return; }
        }

        public class SwapTurn : ApplyPenalty {
            public SwapTurn() {
                duration = 10f;
                cooldown = 5f;
            }

            public float duration { get; }

            public float cooldown { get; }

            void ApplyPenalty.Apply(CarController car) {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");

                car.Move(-h, v, v, handbrake);
            }

            void ApplyPenalty.CancelPenalty(CarController car) { return; }
        }

        public class SlowTurn : ApplyPenalty {
            public SlowTurn() {
                duration = 10f;
                cooldown = 5f;
            }

            public float duration { get; }

            public float cooldown { get; }

            void ApplyPenalty.Apply(CarController car) {
                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");

                car.m_SteerPenalty = 0.3f;

                car.Move(h, v, v, handbrake);
            }

            void ApplyPenalty.CancelPenalty(CarController car) {
                car.m_SteerPenalty = 1f;
            }
        }

        public class FadeScreen : ApplyPenalty {
            private bool ran;
            private Fade fade;

            public FadeScreen(Fade fade) {
                duration = 5f;
                cooldown = 1f;

                ran = false;
                this.fade = fade;
            }

            public float duration { get; }

            public float cooldown { get; }

            void ApplyPenalty.Apply(CarController car) {
                if (!ran) {
                    fade.FadeMe();
                    ran = true;
                }

                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");

                car.Move(h, v, v, handbrake);
            }

            void ApplyPenalty.CancelPenalty(CarController car) {
                fade.UnfadeMe();
                ran = false;
            }
        }

        public class DrunkASFPenalty : ApplyPenalty {
            private bool ran;
            private DrunkASF drunkASF;

            public DrunkASFPenalty(DrunkASF drunkASF) {
                duration = 10f;
                cooldown = 5f;

                this.drunkASF = drunkASF;

                ran = false;
            }

            public float duration { get; }

            public float cooldown { get; }    

            void ApplyPenalty.Apply(CarController car) {
                if (!ran) {
                    ran = true;
                    drunkASF.gigaDrunk = true;
                }

                float h = CrossPlatformInputManager.GetAxis("Horizontal");
                float v = CrossPlatformInputManager.GetAxis("Vertical");
                float handbrake = CrossPlatformInputManager.GetAxis("Jump");

                car.Move(h, v, v, handbrake);
            }

            void ApplyPenalty.CancelPenalty(CarController car) {
                drunkASF.gigaDrunk = false;
                ran = false;
            }
        }
    }
}