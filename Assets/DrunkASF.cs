using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrunkASF : MonoBehaviour
{
    public Material drunkASF;
    public Material drunk;
    public bool gigaDrunk;

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (gigaDrunk) {
            Graphics.Blit(source, destination, drunkASF);
        } else {
            Graphics.Blit(source, destination, drunk);
        }
    }
}
