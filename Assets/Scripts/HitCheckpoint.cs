using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitCheckpoint : MonoBehaviour {
    public bool m_ReachedEnd = false;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "PlayerCollider") {
            UnityEngine.Debug.Log("Checkpoint!");
            m_ReachedEnd = true;
        }
    }
}
