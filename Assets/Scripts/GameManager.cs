using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject m_checkPointController;
    [SerializeField] private Text timerText;
    [SerializeField] private Text timeTakenText;
    [SerializeField] private Text timeTakeValue;
    [SerializeField] private Text personKilledText;
    [SerializeField] private Text personKilledValue;
    [SerializeField] private Text boireOuConduire;
    [SerializeField] private Text ilFautChoisir;
    [SerializeField] private GameObject player;
    [SerializeField] private GameObject cutScene;

    private CheckPointController checkpointController;

    private bool end;
    private double gameTime;
    private double cinematicTime;
    private bool secondphase;

    void Start() {
        checkpointController = m_checkPointController.GetComponent<CheckPointController>();

        gameTime = Time.time;
        end = false;
        secondphase = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!end) {
            if (checkpointController.reachedEnd) {
                // END OF THE GAME
                end = true;
                gameTime = Time.time - gameTime;
                TimeSpan.FromMilliseconds(gameTime);
                UnityEngine.Debug.Log("Reached the end of the game.");
                player.GetComponent<UnityStandardAssets.Vehicles.Car.CarAudio>().StopSound();
                UnityStandardAssets.Vehicles.Car.CarInputsController cip = player.GetComponent<UnityStandardAssets.Vehicles.Car.CarInputsController>();
                if (cip.currentPenalty != null) {
                    cip.currentPenalty.CancelPenalty(player.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>());
                    cip.currentPenalty = null;
                }
                player.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>().enabled = false;
                cip.enabled = false;
                timerText.enabled = false;
                cinematicTime = Time.time;
                cutScene.SetActive(true);
            } else {
                TimeSpan ts = TimeSpan.FromSeconds(Time.time - gameTime);
                string elapsed = string.Format("{0:D2}:{1:D2}", ts.Minutes, ts.Seconds);

                timerText.text = elapsed;
            }
        } else {
            double currentTime = Time.time - cinematicTime;
            if (currentTime < 10f) {
                return;
            } else if (!secondphase && !timeTakenText.enabled && currentTime >= 15f) {
                timeTakenText.enabled = true;
            } else if (!secondphase && !timeTakeValue.enabled && currentTime >= 17f) {
                TimeSpan ts = TimeSpan.FromSeconds(gameTime);
                timeTakeValue.text = string.Format("{0:D2} minutes et {1:D2} secondes", ts.Minutes, ts.Seconds);
                timeTakeValue.enabled = true;
            } else if (!personKilledText.enabled && currentTime >= 20f) {
                personKilledText.enabled = true;
            } else if (!personKilledValue.enabled && currentTime >= 23f) {
                personKilledValue.enabled = true;
            } else if (!secondphase && currentTime >= 30f) {
                timeTakenText.enabled = false;
                timeTakeValue.enabled = false;
                secondphase = true;
            } else if (!boireOuConduire.enabled && currentTime >= 35f) {
                boireOuConduire.enabled =  true;
            } else if (!ilFautChoisir.enabled && currentTime >= 40f) {
                ilFautChoisir.enabled = true;
            } else if (currentTime >= 50f) {
                SceneManager.LoadScene("MainMenu");
            }
        }
    }
}
